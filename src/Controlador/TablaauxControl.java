
package Controlador;

import Entidades.TablaauxTo;
import java.util.List;
import javax.swing.JOptionPane;
import procesoDao.TablaauxDao;
import utilitarios.Constante;


public class TablaauxControl {
     //variable

    TablaauxDao dao;
    //constructor
      public TablaauxControl() {
        dao = new TablaauxDao();
    }
      
        //metodos de negocio
    public List<TablaauxTo> TablaauxListar() throws Exception {
        //JOptionPane.showMessageDialog(null, "[vacio]");
        return dao.readAll();
    }
    
    public List<TablaauxTo> TablaauxGrupo(String desc) throws Exception {
        // JOptionPane.showMessageDialog(null, "[¿"+desc+"?]");
        return dao.readAll(desc);
    }
    
      public String CodigodeTablaaux(String descripcion) throws Exception {
        return dao.findForName(descripcion);
        
    }
      
      public TablaauxTo BuscarEnGrupo(Object g, Object c) throws Exception{
            return dao.findForId(g, c);
    }        
      
     public String TablaauxBuscar(String x, int grp, int op) throws Exception{
        return dao.findForId(x, grp, op);
    } 
     
       public String TablaauxProcesar(TablaauxTo emp, int op) throws Exception {
        String msg = "";
        switch (op) {
            case Constante.ADD:
                dao.create(emp);
                msg = "Item registrado con exito";
                break;
            case Constante.UPD:
                dao.update(emp);
                msg = "Item actualizado con exito";
                break;
            case Constante.DEL:
                dao.delete(emp);
                msg = "Item eliminado con exito";
                break;
        }
        return msg;
    }  
}
