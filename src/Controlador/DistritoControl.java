package Controlador;

import Entidades.DistritoTo;
import java.util.List;
import procesoDao.DistritoDao;
import utilitarios.Constante;

public class DistritoControl {
    //variable

    DistritoDao dao;

    //constructor
    public DistritoControl() {
        dao = new DistritoDao();
    }

    //metodos de negocio
    public List<DistritoTo> DistritoListar() throws Exception {
        return dao.readAll();
    }

    public List<DistritoTo> DistritoListaxnombre(String nom) throws Exception {
        return dao.readAll(nom);
    }

    public DistritoTo DistritoPorId(Object id) throws Exception {
        return dao.findForId(id);
    }

    public String DistritoBuscar(String x, int op) throws Exception {
        return dao.findForId(x, op);
    }

    public String DistritoProcesar(DistritoTo emp, int op) throws Exception {
        String msg = "";
        switch (op) {
            case Constante.ADD:
                dao.create(emp);
                msg = "Distrito registrado con exito";
                break;
            case Constante.UPD:
                dao.update(emp);
                msg = "Distrito actualizado con exito";
                break;
            case Constante.DEL:
                dao.delete(emp);
                msg = "Distrito eliminado con exito";
                break;
        }
        return msg;
    }

}
