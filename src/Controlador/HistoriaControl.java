
package Controlador;

import Entidades.HistoriaTo;
import java.util.List;
import procesoDao.HistoriaDao;
import utilitarios.Constante;


public class HistoriaControl {
    //variable de clase HistoriaDao
    HistoriaDao dao;
    
    //constructor
    public HistoriaControl() {
        dao = new HistoriaDao();
    }
    
    //metodos de negocio
    public List<HistoriaTo> HistoriaListar() throws Exception {
        return dao.readAll();
    }

    public List<HistoriaTo> HistoriaListar(String nom) throws Exception {
        return dao.readAll(nom);
    }

    public List<HistoriaTo> HistoriaListar(int id) throws Exception {
        return dao.readAll(id);
    }

    public HistoriaTo HistoriaBuscar(Object id) throws Exception {
        return dao.findForId(id);
    }
    
 
    public String HistoriaProcesar(HistoriaTo emp, int op) throws Exception {
        String msg = "";
        switch (op) {
            case Constante.ADD:
                dao.create(emp);
                msg = "Historia registrado con exito";
                break;
            case Constante.UPD:
                dao.update(emp);
                msg = "Historia actualizado con exito";
                break;
            case Constante.DEL:
                dao.delete(emp);
                msg = "Historia eliminado con exito";
                break;
        }
        return msg;
    }
    
 public String Stringzeros(String hc) throws Exception{
      String hist = "00000000"+Integer.parseInt(hc);
      int nl= hist.length();				//0000000034567     13
      String id = hist.substring(nl-8);
      //"Javadesdecero.es".length(); // retorna 16    devuelve cant caracteres
      //"Javadesdecero.es".substring(4); // retorna desdecero.es  posicion 4+1 al final
      //"Javadesdecero.es".substring(4,9); // retorna desde   Devuelve la subcadena del índice 4 a 9-1.
      return id;
  }  
}
