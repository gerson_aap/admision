package Controlador;

import Entidades.PaisTo;
import java.util.List;
import procesoDao.PaisDao;
import utilitarios.Constante;

public class PaisControl {

    //variable
    PaisDao dao;

    //constructor
    public PaisControl() {
        dao = new PaisDao();
    }

    //metodos de negocio
    public List<PaisTo> PaisListar() throws Exception {
        return dao.readAll();
    }

    public PaisTo PaisPorId(Object id) throws Exception {
        return dao.findForId(id);
    }

    public String PaisBuscar(String x, int op) throws Exception {
        return dao.findForId(x, op);
    }

    public String PaisProcesar(PaisTo emp, int op) throws Exception {
        String msg = "";
        switch (op) {
            case Constante.ADD:
                dao.create(emp);
                msg = "Pais registrado con exito";
                break;
            case Constante.UPD:
                dao.update(emp);
                msg = "Pais actualizado con exito";
                break;
            case Constante.DEL:
                dao.delete(emp);
                msg = "Pais eliminado con exito";
                break;
        }
        return msg;
    }

}
