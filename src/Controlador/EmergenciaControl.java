    
package Controlador;

import Entidades.EmergenciaTo;
import java.util.List;
import procesoDao.EmergenciaDao;
import utilitarios.Constante;


public class EmergenciaControl {
     //variable de clase EmergenciaDao
    EmergenciaDao dao;
    
     //constructor
    public EmergenciaControl() {
        dao = new EmergenciaDao();
        
    }
    
    //metodos de negocio
    public List<EmergenciaTo> EmergenciaListar() throws Exception {
        return dao.readAll();
    }

    public List<EmergenciaTo> EmergenciaListar(String nom) throws Exception {
        return dao.readAll(nom);
    }

    public List<EmergenciaTo> EmergenciaListar(int id) throws Exception {
        return dao.readAll(id);
    }

    public EmergenciaTo EmergenciaBuscar(Object id) throws Exception {
        return dao.findForId(id);
    }
    
    public String EmergenciaProcesar(EmergenciaTo adm, int op) throws Exception {
        String msg = "";
        switch (op) {
            case Constante.ADD:
                dao.create(adm);
                msg = "Emergencia registrada con exito";
                break;
            case Constante.UPD:
                dao.update(adm);
                msg = "Emergencia actualizada con exito";
                break;
            case Constante.DEL:
                dao.delete(adm);
                msg = "Emergencia eliminada con exito";
                break;
        }
        return msg;
    }
}
