/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Entidades.UsuarioTo;
import java.util.List;
import procesoDao.UsuarioDao;
import utilitarios.Session;

/**
 *
 * @author walte
 */
public class UsuarioControl {
       //variable de la clase empleadoDao

    UsuarioDao dao;
    //constructor
     public UsuarioControl() {
        dao = new UsuarioDao();
    }

    public List<UsuarioTo> UsuarioListar() throws Exception {
        return dao.readAll();
    }

    public UsuarioTo UsuarioBuscar(Object id) throws Exception {
        return dao.findForId(id);
    }
 public boolean UsuarioValidar(String usua, String pas) throws Exception {
        UsuarioTo usu = dao.Validar(usua, pas);
        boolean ok;
        if (usu != null) {
            Session.put("usuario", usu);
            ok = true;
        } else {
            ok = false;
        }
        return ok;
    }

}
