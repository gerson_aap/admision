
package Vistas;
import java.awt.BorderLayout;
import java.awt.Container;
import java.sql.Connection;
import java.util.HashMap;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import ConexionBD.AccesoDB;

public class InformeView extends javax.swing.JInternalFrame {

    public InformeView() {
        super("Reporteador Interno", true, true, true, true);
        initComponents();
        setBounds(10, 10, 600, 500);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    

 public InformeView(String fileName) {
  this(fileName, null);
 }
 
   public InformeView(String fileName, HashMap parameter) { 
        this(); 
        try{ 
            Connection con=AccesoDB.getConnection(); 
            JasperPrint print=JasperFillManager.fillReport(fileName, parameter,con); 
            JRViewer viewer =new JRViewer(print); 
            Container c=getContentPane(); 
            c.setLayout(new BorderLayout()); 
            c.add(viewer); 
        } 
        catch(Exception jre){ 
        } 
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 394, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 274, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
