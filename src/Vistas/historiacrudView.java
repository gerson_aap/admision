package Vistas;

import Controlador.DistritoControl;
import Controlador.HistoriaControl;
import Controlador.PaisControl;
import Controlador.TablaauxControl;
import Entidades.DistritoTo;
import Entidades.HistoriaTo;
import Entidades.PaisTo;
import Entidades.TablaauxTo;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import utilitarios.Grupotaux;

public class historiacrudView extends javax.swing.JInternalFrame {

    public historiacrudView() {
        initComponents();

        try {
            SimpleDateFormat mascara = new SimpleDateFormat("dd/MM/yyyy");
            java.util.Date hoy = new Date();
            lblfecha.setText(mascara.format(hoy));

            cargaListas();
        } catch (Exception e) {
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txhistoria = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        btnAdicionar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        txapelpat = new javax.swing.JTextField();
        txapelmat = new javax.swing.JTextField();
        txnombres = new javax.swing.JTextField();
        txfecnac = new javax.swing.JTextField();
        txdireccion = new javax.swing.JTextField();
        txtelefono = new javax.swing.JTextField();
        txnrodoi = new javax.swing.JTextField();
        cbotdoi = new javax.swing.JComboBox<>();
        cbopais = new javax.swing.JComboBox<>();
        btnLimpiar = new javax.swing.JButton();
        txdistrito = new javax.swing.JTextField();
        cbosexo = new javax.swing.JComboBox<>();
        cbodistrito = new javax.swing.JComboBox<>();
        lblfecha = new javax.swing.JLabel();
        lblprov = new javax.swing.JLabel();

        setTitle("MANTENIMIENTO DE HISTORIAS");

        jLabel1.setText("Codigo");

        jLabel2.setText("Ap. Paterno");

        jLabel3.setText("Ap. Materno");

        jLabel4.setText("Nombres");

        jLabel5.setText("Sexo");

        jLabel6.setText("Fec. Nacimiento");

        jLabel7.setText("Dirección");

        jLabel8.setText("Distrito");

        jLabel9.setText("Tpo. Documento");

        jLabel10.setText("Número");

        jLabel11.setText("Pais");

        jLabel12.setText("Telefono");

        btnAdicionar.setText("Adicionar");
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        cbodistrito.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbodistritoItemStateChanged(evt);
            }
        });
        cbodistrito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbodistritoActionPerformed(evt);
            }
        });

        lblfecha.setText("jLabel13");

        lblprov.setText("jlabelprov");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txapelpat)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txhistoria, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(31, 31, 31)
                                        .addComponent(btnBuscar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lblfecha, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txnombres)
                                    .addComponent(txapelmat)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txfecnac, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)))
                                .addGap(35, 35, 35)))
                        .addGap(50, 50, 50))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(txtelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cbosexo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(328, 328, 328))
                                    .addComponent(txdireccion)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addGap(18, 18, 18)
                                        .addComponent(cbotdoi, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(58, 58, 58)
                                        .addComponent(txdistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cbodistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel10)
                                        .addGap(18, 18, 18)
                                        .addComponent(txnrodoi, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(25, 25, 25))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(lblprov, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(8, 8, 8)))))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbopais, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(btnAdicionar)
                                .addGap(27, 27, 27)
                                .addComponent(btnModificar)
                                .addGap(33, 33, 33)
                                .addComponent(btnEliminar)
                                .addGap(27, 27, 27)
                                .addComponent(btnLimpiar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCerrar)
                        .addGap(40, 40, 40))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txhistoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addComponent(lblfecha))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txapelpat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txapelmat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txnombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(txfecnac, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbosexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblprov, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(txdistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cbodistrito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cbotdoi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txnrodoi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbopais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdicionar)
                    .addComponent(btnModificar)
                    .addComponent(btnEliminar)
                    .addComponent(btnCerrar)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        procesar(1);
        limpiar();
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        procesar(2);
        limpiar();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        procesar(4);
        limpiar();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
          consulta();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void cbodistritoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbodistritoActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_cbodistritoActionPerformed

    private void cbodistritoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbodistritoItemStateChanged
        try {
            mostrarDistrito();
        } catch (Exception e) {
            //  JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }//GEN-LAST:event_cbodistritoItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox<String> cbodistrito;
    private javax.swing.JComboBox<String> cbopais;
    private javax.swing.JComboBox<String> cbosexo;
    private javax.swing.JComboBox<String> cbotdoi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblfecha;
    private javax.swing.JLabel lblprov;
    private javax.swing.JTextField txapelmat;
    private javax.swing.JTextField txapelpat;
    private javax.swing.JTextField txdireccion;
    private javax.swing.JTextField txdistrito;
    private javax.swing.JTextField txfecnac;
    private javax.swing.JTextField txhistoria;
    private javax.swing.JTextField txnombres;
    private javax.swing.JTextField txnrodoi;
    private javax.swing.JTextField txtelefono;
    // End of variables declaration//GEN-END:variables
//instanciar objetos
    TablaauxControl opro = new TablaauxControl();
    PaisControl opais = new PaisControl();
    DistritoControl odistrito = new DistritoControl();

    private void cargaListas() throws Exception {
        for (PaisTo c : opais.PaisListar()) {
            cbopais.addItem(c.getNombrePais());
        }
        cbopais.setSelectedIndex(-1);

        String dsc = "DOC";
        for (TablaauxTo d : opro.TablaauxGrupo(dsc)) {
            cbotdoi.addItem(d.getDescripcion());
        }
        cbotdoi.setSelectedIndex(-1);

        String desc = "SEX";
        for (TablaauxTo p : opro.TablaauxGrupo(desc)) {
            cbosexo.addItem(p.getDescripcion());
        }
        cbosexo.setSelectedIndex(-1);

        for (DistritoTo x : odistrito.DistritoListar()) {
            cbodistrito.addItem(x.getDistrito() + " - " + x.getUbi_geogr().trim());
        }
        cbodistrito.setSelectedIndex(-1);
        txdistrito.setText("");
    }

    //crear objeto de la clase historiacontrol
    HistoriaControl obj = new HistoriaControl();
    HistoriaTo pro;

    private void procesar(int op) {
        try {
            pro = leerDatos();
            String msg = obj.HistoriaProcesar(pro, op);
            JOptionPane.showMessageDialog(null, msg);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void consulta() {
        try {
            
            String hc ="00000000"+txhistoria.getText();
            Integer nlen = hc.length();
            String hist = hc.substring(nlen-8);
            txhistoria.setText(hist);
            pro = obj.HistoriaBuscar(txhistoria.getText());
            
            if (pro != null) {
                cbosexo.setSelectedIndex(-1);
                cbodistrito.setSelectedIndex(-1);
                cbotdoi.setSelectedIndex(-1);
                cbopais.setSelectedIndex(-1);
                txhistoria.setText(pro.getHistoria());
                txapelpat.setText(pro.getApelPat());
                txapelmat.setText(pro.getApelMat());
                txnombres.setText(pro.getNombres());
                // JOptionPane.showMessageDialog(null, "[" + pro.getSexo() + "]");
                // JOptionPane.showMessageDialog(null, "[-" + opro.TablaauxBuscar(pro.getSexo(), Grupotaux.SEX, 2) + "-]");
                cbosexo.setSelectedItem(opro.TablaauxBuscar(pro.getSexo(), Grupotaux.SEX, 2));
                //---GET  Date Fecnac  => String txfecnac 
                java.util.Date hoy = new Date();
                SimpleDateFormat mascara = new SimpleDateFormat("dd/MM/yyyy");
                lblfecha.setText(mascara.format(hoy));
                if (pro.getFecNac() != null) {
                    Date fnac = pro.getFecNac();
                    txfecnac.setText(mascara.format(fnac));
                }
                //txtcantidad.setText(pro.getStock() + "");
                txdistrito.setText(pro.getDistrito());
                cbodistrito.setSelectedItem(odistrito.DistritoBuscar(pro.getDistrito(), 2) + " - " + pro.getDistrito());
                lblprov.setText(odistrito.DistritoBuscar(pro.getDistrito(), 3));
                txdireccion.setText(pro.getDireccion());
                txtelefono.setText(pro.getTelefono());
                txnrodoi.setText(pro.getNroDoi());
//                txtpodoi.setText(pro.getTpoDoi());
//                JOptionPane.showMessageDialog(null, "[" + pro.getTpoDoi() + "]");
//                txcodpais.setText(pro.getCodPais());
                cbotdoi.setSelectedItem(opro.TablaauxBuscar(pro.getTpoDoi(), Grupotaux.DOC, 2));
                if (pro.getCodPais() != null && pro.getCodPais() != "    ") {
                    cbopais.setSelectedItem(opais.PaisBuscar(pro.getCodPais(), 2));
                }
            } else {
                JOptionPane.showMessageDialog(null, "Historia no existe");
            }
        } catch (Exception e) {
          //  JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }

    private HistoriaTo leerDatos() {
        try {
            pro = new HistoriaTo();
            //asignar valores al objeto pro
            pro.setHistoria(txhistoria.getText());
            pro.setApelPat(txapelpat.getText());
            pro.setApelMat(txapelmat.getText());
            pro.setNombres(txnombres.getText());
            // JOptionPane.showMessageDialog(null,cbosexo.getSelectedItem().toString().substring(0,1));
            String csex = cbosexo.getSelectedItem().toString().substring(0, 1);
            pro.setSexo(csex);
            pro.setDireccion(txdireccion.getText());
            pro.setDistrito(txdistrito.getText());
            pro.setTelefono(txtelefono.getText());
            //JOptionPane.showMessageDialog(null,cbotdoi.getSelectedItem().toString());
            String ctdoi = opro.TablaauxBuscar(cbotdoi.getSelectedItem().toString(), Grupotaux.DOC, 1);
            //JOptionPane.showMessageDialog(null,ctdoi);
            pro.setTpoDoi(ctdoi);
            pro.setNroDoi(txnrodoi.getText());
            pro.setCodPais(opais.PaisBuscar(cbopais.getSelectedItem().toString(), 1));
        } catch (Exception e) {
        }

          SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
          java.util.Date fecha = null;
          java.sql.Date fecha2 = null;
          String infn = txfecnac.getText();  
          try {
//              JOptionPane.showMessageDialog(null, infn.substring(0,2));
//              JOptionPane.showMessageDialog(null, infn.substring(3,5));
//              JOptionPane.showMessageDialog(null, infn.substring(6,10));
            String fnac =  infn.substring(6,10)  +"-"+ infn.substring(3,5) +"-"+infn.substring(0,2);    //dd/mm/yyyy
            fecha = df.parse( fnac);       // convierte el string en util.Date
            fecha2 = new java.sql.Date(fecha.getTime()); // convierte el util.Date en sql.Date
            pro.setFecNac(fecha2);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

        //pro.setPrecioCompra(Double.parseDouble(txtprecompra.getText()));
        //pro.setPrecioVenta(pro.getPrecioCompra() * 1.30);
        //pro.setStock(Integer.parseInt(txtcantidad.getText()));
        return pro;
    }

    private void limpiar() {
        //Date hoy = new Date();
        //SimpleDateFormat mascara = new SimpleDateFormat("dd/MM/yyyy");
        //lblfecha.setText(mascara.format(hoy));
        txhistoria.setText("");
        txapelpat.setText("");
        txapelmat.setText("");
        txnombres.setText("");
        cbosexo.setSelectedIndex(-1);
        txfecnac.setText("");
        txdistrito.setText("");
        cbodistrito.setSelectedIndex(-1);
        lblprov.setText("");
        txdireccion.setText("");
        txtelefono.setText("");
        //lista.clear();
        //DefaultTableModel model = (DefaultTableModel) tbdetalle.getModel();
        //model.setRowCount(0);
        cbotdoi.setSelectedIndex(-1);
        txnrodoi.setText("");
        cbopais.setSelectedIndex(-1);
    }

    private void mostrarDistrito() {
        try {
            String cadena = cbodistrito.getSelectedItem().toString().trim();
            // JOptionPane.showMessageDialog(null, cadena);
            int nl = cadena.length();
            String id = cadena.substring(nl - 6);

            //JOptionPane.showMessageDialog(null, id);
            DistritoTo x = odistrito.DistritoPorId(id);
            //set datos del distrito
            txdistrito.setText(x.getUbi_geogr());
            lblprov.setText(x.getProv());
            txtelefono.grabFocus();
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }

 //-----------------  OK -----------
//          SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//          java.util.Date fecha = null;
//          java.sql.Date fecha2 = null;
//          //String datafn = txfecnac.getText();  
//          try {
//              fecha = df.parse( "2010-10-25");       // convierte el string en util.Date
//              fecha2 = new java.sql.Date(fecha.getTime()); // convierte el util.Date en sql.Date
//          
//            pro.setFecNac(fecha2);
//        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(null, ex.getMessage());
//        }
   
 //--------------   
  
//dataNascita = "07/09/1969";
//sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
//data = null;
//try {
//      data = sdf.parse(dataNascita);
//      dataNascita = new SimpleDateFormat("dd/MM/yyyy").format(data);
//}catch (ParseException e) {
//      System.err.println(e);
//}
//System.out.println(dataNascita); 
    
//----------------       
//String dataNascita = "1969-09-07 00:00:00.0";
//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
//Date data = null;
//try {
//    data = sdf.parse(dataNascita);
//    dataNascita = new SimpleDateFormat("dd/MM/yyyy").format(data);
//}catch (ParseException e) {
//    System.err.println(e);
//}
//System.out.println(dataNascita);   
//-------------
    
    
//public static String formatDate (String date, String initDateFormat, String endDateFormat) throws ParseException {
//
//       Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
//       SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
//       String parsedDate = formatter.format(initDate);
//
//       return parsedDate;
//}

//----------------
    /**
     * convertir String en fecha
     *
     * @param fecha cadena fecha dd/mm/yyyy
     * @return Object Date
     */
//public static Date ParseFecha(String fecha)
//{
//	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
//	Date fechaDate = null;
//	try {
//		fechaDate = formato.parse(fecha);
//	}
//	catch (ParseException ex)
//	}
//  		System.out.println(ex);
//	}
//	return fechaDate;
//}
    
//--------------------    
    //public static String fechaActual(){
    //  Date fechaActual = new Date(); 
    //  SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd"); 
    //  String fecha = formato.format(fechaActual);
    //   return fecha;
    //}
    //  Date fnac = pro.getFecNac();
    // SimpleDateFormat mascara = new SimpleDateFormat("dd/MM/yyyy");
    //  lblfecha.setText(mascara.format(hoy));
    //  txfecnac.setText(mascara.format(fnac));
}
