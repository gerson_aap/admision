
package servicios;

import java.util.List;


public interface ICrudService<T> {
   //definir las firmas

    void create(T t) throws Exception;

    void update(T t) throws Exception;

    void delete(T t) throws Exception;

    List<T> readAll() throws Exception;

    T findForId(Object t) throws Exception;  
}
