package Entidades;

public class DistritoTo {

    private String ubi_geogr;
    private String distrito;
    private String prov;
    private String depto;

    // constructor
    public DistritoTo() {
    }

    public DistritoTo(String ubi_geogr, String distrito, String prov, String depto) {
        this.ubi_geogr = ubi_geogr;
        this.distrito = distrito;
        this.prov = prov;
        this.depto = depto;
    }

    public String getUbi_geogr() {
        return ubi_geogr;
    }

    public void setUbi_geogr(String ubi_geogr) {
        this.ubi_geogr = ubi_geogr;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }

    @Override
    public String toString() {
        return distrito + ", " + prov + ", " + depto;
    }

}
