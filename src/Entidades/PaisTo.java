
package Entidades;

public class PaisTo {
    //atributos
    private String codPais;
    private String nombrePais;
   //constructores

    public PaisTo() {
    }

    public PaisTo(String codPais, String nombrePais) {
        this.codPais = codPais;
        this.nombrePais = nombrePais;
    }
    
     //metodos get y set

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    @Override
    public String toString() {
        return nombrePais;
    }
    
}
