package Entidades;

import java.util.Date;

public class EmergenciaTo extends HistoriaTo {

    private Date fechaIng;
    private String horaIng;
    private String catPac;
    private String NroFua;
    private String gravedad;
    private String prioridad;
    private String MotIng;
    private String tipAte;
    private String tpoAco;
    private String acompanante;
    private String atpoDoi;
    private String anroDoi;
    private String observacion;
    private String secuencia;

    public EmergenciaTo() {
    }

    public EmergenciaTo(String historia, String apelPat, String apelMat, String nombres, String sexo, Date fecNac, String tpoDoi, String nroDoi, String codPais, String direccion, String distrito, String telefono, Date fechaIng, String horaIng, String catPac, String NroFua, String gravedad, String prioridad, String MotIng, String tipAte, String tpoAco, String acompanante, String atpoDoi, String anroDoi, String observacion, String secuencia) {
        super(historia, apelPat, apelMat, nombres, sexo, fecNac, tpoDoi, nroDoi, codPais, direccion, distrito, telefono); //llama a constructor de la clase historiaTo
        this.fechaIng = fechaIng;
        this.horaIng = horaIng;
        this.catPac = catPac;
        this.NroFua = NroFua;
        this.gravedad = gravedad;
        this.prioridad = prioridad;
        this.MotIng = MotIng;
        this.tipAte = tipAte;
        this.tpoAco = tpoAco;
        this.acompanante = acompanante;
        this.atpoDoi = atpoDoi;
        this.anroDoi = anroDoi;
        this.observacion = observacion;
        this.secuencia = secuencia;
    }

    public Date getFechaIng() {
        return fechaIng;
    }

    public void setFechaIng(Date fechaIng) {
        this.fechaIng = fechaIng;
    }

    public String getHoraIng() {
        return horaIng;
    }

    public void setHoraIng(String horaIng) {
        this.horaIng = horaIng;
    }

//    public String getHistoria() {
//      return historia;
//    }
//
//    public void setHistoria(String historia) {
//        this.historia = historia;
//    }
//
//    public String getApelPat() {
//        return apelPat;
//    }
//
//    public void setApelPat(String apelPat) {
//        this.apelPat = apelPat;
//    }
//
//    public String getApelMat() {
//        return apelMat;
//    }
//
//    public void setApelMat(String apelMat) {
//        this.apelMat = apelMat;
//    }
//
//    public String getNombres() {
//        return nombres;
//    }
//
//    public void setNombres(String nombres) {
//        this.nombres = nombres;
//    }
//
//    public Date getFecNac() {
//        return fecnac;
//    }
//
//    public void setFecNac(Date fecnac) {
//        this.fecnac = fecnac;
//    }
//
//    public String getSexo() {
//        return sexo;
//    }
//
//    public void setSexo(String sexo) {
//        this.sexo = sexo;
//    }
//
//    public String getTpoDoi() {
//        return tpoDoi;
//    }
//
//    public void setTpoDoi(String tpoDoi) {
//        this.tpoDoi = tpoDoi;
//    }
//
//    public String getNroDoi() {
//        return nroDoi;
//    }
//
//    public void setNroDoi(String nroDoi) {
//        this.nroDoi = nroDoi;
//    }
//
//    public String getDistrito() {
//        return distrito;
//    }
//
//    public void setDistrito(String distrito) {
//        this.distrito = distrito;
//    }
//
//    public String getDireccion() {
//        return direccion;
//    }
//
//    public void setDireccion(String direccion) {
//        this.direccion = direccion;
//    }
//
//    public String getTelefono() {
//        return telefono;
//    }
//
//    public void setTelefono(String telefono) {
//        this.telefono = telefono;
//    }
    public String getCatPac() {
        return catPac;
    }

    public void setCatPac(String catPac) {
        this.catPac = catPac;
    }

    public String getNroFua() {
        return NroFua;
    }

    public void setNroFua(String NroFua) {
        this.NroFua = NroFua;
    }

    public String getGravedad() {
        return gravedad;
    }

    public void setGravedad(String gravedad) {
        this.gravedad = gravedad;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getMotIng() {
        return MotIng;
    }

    public void setMotIng(String MotIng) {
        this.MotIng = MotIng;
    }

    public String getTipAte() {
        return tipAte;
    }

    public void setTipAte(String tipAte) {
        this.tipAte = tipAte;
    }

   public String getTpoAco() {
        return tpoAco;
    }

    public void setTpoAco(String tpoAco) {
        this.tpoAco = tpoAco;
    }

public String getAcompanante() {
        return acompanante;
    }

    public void setAcompanante(String acompanante) {
        this.acompanante = acompanante;
    }

public String getAtpoDoi() {
        return atpoDoi;
    }

    public void setAtpoDoi(String atpoDoi) {
        this.atpoDoi = atpoDoi;
    }

public String getAnroDoi() {
        return anroDoi;
    }

    public void setAnroDoi(String anroDoi) {
        this.anroDoi = anroDoi;
    } 
    
    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public String toString() {
        return "EmergenciaTo{" + "fechaIng=" + fechaIng + ", horaIng=" + horaIng + ", historia=" + super.getHistoria() + '}';
    }

}
