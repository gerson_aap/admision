/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author walte
 */
public class UsuarioTo {

    private String usuario;
    private String nombre;
    private String derechos;
    private String acceso1;
    private String acceso2;
    private String acceso3;
    private String acceso4;
    private String acceso5;
    private String acceso6;
    private String acceso7;
    private String clave;
    private String cambia;

    public UsuarioTo() {
    }

    public UsuarioTo(String usuario, String nombre, String derechos, String acceso1, String acceso2, String acceso3, String acceso4, String acceso5, String acceso6, String acceso7, String clave, String cambia) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.derechos = derechos;
        this.acceso1 = acceso1;
        this.acceso2 = acceso2;
        this.acceso3 = acceso3;
        this.acceso4 = acceso4;
        this.acceso5 = acceso5;
        this.acceso6 = acceso6;
        this.acceso7 = acceso7;
        this.clave = clave;
        this.clave = cambia;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDerechos() {
        return derechos;
    }

    public void setDerechos(String derechos) {
        this.derechos = derechos;
    }

    public String getAcceso1() {
        return acceso1;
    }

    public void setAcceso1(String acceso1) {
        this.acceso1 = acceso1;
    }

    public String getAcceso2() {
        return acceso2;
    }

    public void setAcceso2(String acceso2) {
        this.acceso2 = acceso2;
    }

    public String getAcceso3() {
        return acceso3;
    }

    public void setAcceso3(String acceso3) {
        this.acceso3 = acceso3;
    }

    public String getAcceso4() {
        return acceso4;
    }

    public void setAcceso4(String acceso4) {
        this.acceso4 = acceso4;
    }

    public String getAcceso5() {
        return acceso5;
    }

    public void setAcceso5(String acceso5) {
        this.acceso5 = acceso5;
    }

    public String getAcceso6() {
        return acceso6;
    }

    public void setAcceso6(String acceso6) {
        this.acceso6 = acceso6;
    }

    public String getAcceso7() {
        return acceso7;
    }

    public void setAcceso7(String acceso7) {
        this.acceso7 = acceso7;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    
    public String getCambia() {
        return cambia;
    }

    public void setCambia(String cambia) {
        this.cambia = cambia;
    }
    
    @Override
    public String toString() {
        return nombre;
    }

    
    
}
