package Entidades;

public class TablaauxTo {

    private String grupo;
    private String codigo;
    private String descripcion;

    public TablaauxTo() {
    }

    public TablaauxTo(String grupo, String codigo, String descripcion) {
        this.grupo = grupo;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }

}
