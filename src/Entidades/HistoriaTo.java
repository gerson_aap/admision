package Entidades;

import java.util.Date;

public class HistoriaTo {

    private String historia;
    private String apelPat;
    private String apelMat;
    private String nombres;
    private String sexo;
    private Date fecNac;
    private String direccion;
    private String distrito;
    private String tpoDoi;
    private String nroDoi;
    private String codPais;
    private String telefono;

    public HistoriaTo() {
    }

    public HistoriaTo(String historia, String apelPat, String apelMat, String nombres, String sexo, Date fecNac, String direccion, String distrito, String tpoDoi, String nroDoi, String codPais, String telefono) {
        this.historia = historia;
        this.apelPat = apelPat;
        this.apelMat = apelMat;
        this.nombres = nombres;
        this.sexo = sexo;
        this.fecNac = fecNac;
        this.direccion = direccion;
        this.distrito = distrito;
        this.tpoDoi = tpoDoi;
        this.nroDoi = nroDoi;
        this.codPais = codPais;
        this.telefono = telefono;
    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public String getApelPat() {
        return apelPat;
    }

    public void setApelPat(String apelPat) {
        this.apelPat = apelPat;
    }

    public String getApelMat() {
        return apelMat;
    }

    public void setApelMat(String apelMat) {
        this.apelMat = apelMat;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getFecNac() {
        return fecNac;
    }

    public void setFecNac(Date fecNac) {
        this.fecNac = fecNac;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTpoDoi() {
        return tpoDoi;
    }

    public void setTpoDoi(String tpoDoi) {
        this.tpoDoi = tpoDoi;
    }

    public String getNroDoi() {
        return nroDoi;
    }

    public void setNroDoi(String nroDoi) {
        this.nroDoi = nroDoi;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return apelPat+" "+apelMat+", "+nombres;
    }
    

    
}
