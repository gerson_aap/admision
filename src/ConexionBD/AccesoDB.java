
package ConexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public final class AccesoDB {
    public static Connection getConnection() throws SQLException, Exception {
         Connection cn = null;
         try {
             // Parámetros de Connexión
              String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
              //String url = "jdbc:sqlserver://HPVALQUI\\SQLEXPRESS:1433;databaseName=hepmaestsql";
              String url = "jdbc:sqlserver://192.168.1.15:1433;databaseName=hepmaestsql";
              String user = "sa";
              String pwd = "P4ssw0rd";
              Class.forName(driver).newInstance();
              cn = DriverManager.getConnection(url, user, pwd);

        } catch (Exception e) {
                throw e;
        }
return cn;
    }
}
