package ConexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Prueba03{

    public static void main(String[] args) throws SQLException {
        // prueba de conexiona BD de oracle
        Connection cn = null;
        try {
            //1 cargar driver en memoria
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            //2 -definir la URL de la Base de datos
            String url = "jdbc:sqlserver://192.168.1.15:1433;databaseName=hepmaestsql";
            //3 obtener la conexion a la BD
            cn = DriverManager.getConnection(url, "sa", "P4ssw0rd");
            //usar el objeto Statement
            String sql="select * from Historias where historia > '00522900' order by apelpat, apelmat";
            //crear objeto Statement
            Statement stm=cn.createStatement();
            //ejecutar consulta
            ResultSet rs=stm.executeQuery(sql);
            //ver lista de productos
            System.out.println("Lista de HISTORIAS");
            while (rs.next()) {                
                System.out.println(rs.getString(1)+"--"+rs.getString(2)+" "+rs.getString(3)+", "+rs.getString(4)+" "+rs.getDate(6));
            }
            rs.close();
            stm.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            System.out.println("Error : " + e.getMessage());
        } finally {
            cn.close();
        }
    }

}
