package ConexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Prueba02 {

    public static void main(String[] args) throws SQLException {
        // prueba de conexiona BD de oracle
        Connection cn = null;
        try {
            //1 cargar driver en memoria
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                        System.out.println("Driver Conforme...");
            //2 -definir la URL de la Base de datos
            String url = "jdbc:sqlserver://HPVALQUI:1433;databaseName=hepmaestsql";
            //3 obtener la conexion a la BD
            cn = DriverManager.getConnection(url, "sa", "P4ssw0rd");
            System.out.println("Conexion Conforme...");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            System.out.println("Error : " + e.getMessage());
        } finally {
            cn.close();
        }
    }

}
