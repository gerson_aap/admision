package procesoDao;

    import ConexionBD.AccesoDB;
    import Entidades.HistoriaTo;
    import java.sql.PreparedStatement;
    import java.sql.CallableStatement;
    import java.sql.Connection;
    import java.sql.Date;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.sql.Statement;
    import java.util.ArrayList;
    import java.util.List;
    import servicios.ICrudService;

public class HistoriaDao implements ICrudService<HistoriaTo> {
    //variables

    Connection cn;
    CallableStatement cs;
    ResultSet rs;
    Statement stm;
    PreparedStatement ps;
    HistoriaTo emp;
    String sp;
    String sql;

    @Override
    public void create(HistoriaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            String cod = generaNroHc();    //genera Nrumero Historia
            t.setHistoria(cod);
            
            // MANEJO CADENAS STRING
            //"Javadesdecero.es".substring(4); // retorna desdecero.es  posicion 4+1 al final
            //"Javadesdecero.es".length(); // retorna 16    devuelve cant caracteres
            //"Javadesdecero.es".substring(4,9); // retorna desde   Devuelve la subcadena del índice 4 a 9-1.
            //System.out.println("Javadesdecero.es".charAt(3)); // retorna 'a'   Devuelve el carácter en el índice 3.

            //String s1 = "Java";   String s2 = "desdeCero;    String salida = s1.concat(s2); // retorna "JavadesdeCero"
            //String s = "Java desde Cero";  int salida = s.indexOf("Cero");  // retorna 11
            //Devuelve el índice dentro de la cadena de la primera aparición de la cadena especificada, comenzando en el índice especificado.
            //String s = "Java desde Cero";  // int salida = s.indexOf('a',3);  //retorna 3
            //Devuelve el índice dentro de la cadena de la última aparición de la cadena especificada.
            //String s = "Java desde Cero";   int salida = s.lastIndexOf('a'); // retorna 3
            //int salida = s1.compareTo(s2); // donde s1 y s2 son strings que se comparan
            //Esto devuelve la diferencia s1-s2. Si : salida < 0 // s1 es menor que s2  
            //salida = 0 // s1 y s2 son iguales       salida > 0 // s1 es mayor que s2
            //String palabra1 = "HoLa";    String palabra2 = palabra1.toLowerCase(); // retorna "hola"
            //String palabra1 = "HoLa";     String palabra2 = palabra1.toUpperCase(); // retorna "HOLA"
            //String palabra1 = " Java desde Cero  ";  String palabra2 = palabra1.trim(); // retorna "Java desde Cero"
            //String palabra1 = "yavadesdecero";      String palabra2 = palabra1.replace('y' ,'j'); //retorna javadesdecero
            //StringBuffer sb = new StringBuffer("Bebe Caliente!");   sb.insert(6, "Java ");  
            //System.out.println(sb.toString());      muestra  Bebe Java Caliente!
            //String piStr = "3.14159";  Float pi = Float.valueOf(piStr);   Convierte Cadena a Numero
          
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call sp_Historia_Adiciona(?,?,?,?,?,?,?,?,?,?,?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getHistoria());
            cs.setString(2, t.getApelPat());
            cs.setString(3, t.getApelMat());
            cs.setString(4, t.getNombres());
            cs.setString(5, t.getSexo());
            cs.setDate(6, (Date) t.getFecNac());
            cs.setString(7, t.getDireccion());
            cs.setString(8, t.getDistrito());
            cs.setString(9, t.getTpoDoi());
            cs.setString(10, t.getNroDoi());
            cs.setString(11, t.getCodPais());
            cs.setString(12, t.getTelefono());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public void update(HistoriaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Historia_Actualiza(?,?,?,?,?,?,?,?,?,?,?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getHistoria());
            cs.setString(2, t.getApelPat());
            cs.setString(3, t.getApelMat());
            cs.setString(4, t.getNombres());
            cs.setString(5, t.getSexo());
            cs.setDate(6, (Date) t.getFecNac());
            cs.setString(7, t.getDireccion());
            cs.setString(8, t.getDistrito());
            cs.setString(9, t.getTpoDoi());
            cs.setString(10, t.getNroDoi());
            cs.setString(11, t.getCodPais());
            cs.setString(12, t.getTelefono());

            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public void delete(HistoriaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Historia_Elimina(?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getHistoria());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public List<HistoriaTo> readAll() throws Exception {
        List<HistoriaTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Historias order by 1";
            stm = cn.createStatement();
            rs = stm.executeQuery(sql);
            emp = null;
            while (rs.next()) {
                emp = new HistoriaTo(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getDate(6),
                        rs.getString(7), rs.getString(8), rs.getString(11),
                        rs.getString(12), rs.getString(13), rs.getString(14));
                listar.add(emp);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;

    }

    @Override
    public HistoriaTo findForId(Object t) throws Exception {
        emp = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Historias where historia=?";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, (String) t);
            rs = ps.executeQuery();
            if (rs.next()) {
                emp = new HistoriaTo(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getDate(6),
                        rs.getString(7), rs.getString(8), rs.getString(11),
                        rs.getString(12), rs.getString(13), rs.getString(14));
                
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return emp;
    }

    public String findForId(String x, int op) throws Exception {
        String dato = null;
        try {
            switch (op) {
                case 1:
                    sql = "select historia from Historias where apelpat='" + x + "'";
                    break;
                case 2:
                    sql = "select apelpat,apelmat,nombres from historias where historia='" + x + "'";
                    break;
            }
            cn = AccesoDB.getConnection();
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();
            dato = rs.getString(1);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return dato;
    }

    public String findForName(String apelp) throws Exception {
        String id = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select historia from historias  where apelpat='" + apelp + "'";
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            //cargar los proveedores de rs a la coleccion lista
            if (rs.next()) {
                id = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return id;
    }

    public List<HistoriaTo> readAll(String apelp) throws Exception {
        List<HistoriaTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from historias where Apelpat like ?";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(2, apelp + "%");
            rs = ps.executeQuery();
            emp = null;
            while (rs.next()) {
                emp = new HistoriaTo(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getDate(6),
                        rs.getString(7), rs.getString(8), rs.getString(11),
                        rs.getString(12), rs.getString(13), rs.getString(14));
                listar.add(emp);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }

    public List<HistoriaTo> readAll(int id) throws Exception {
        List<HistoriaTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from historias where historia=?";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setInt(1, id);
            rs = ps.executeQuery();
            emp = null;
            while (rs.next()) {
                emp = new HistoriaTo(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getDate(6),
                        rs.getString(7), rs.getString(8), rs.getString(11),
                        rs.getString(12), rs.getString(13), rs.getString(14));
                listar.add(emp);

            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }

private String generaNroHc() throws SQLException {
        //leer valor de la columna numero en la tabla numeros
        sql = "select numero from numeros where tipdoc=34";
        ps = cn.prepareStatement(sql);
        rs = ps.executeQuery();
        rs.next();
        int nro = rs.getInt(1);
        rs.close();
        ps.close();
        //actualizar tabla Numeros -> HC
        sql = "update numeros set numero=numero+1 where tipdoc=34";
        ps = cn.prepareStatement(sql);
        ps.executeUpdate();
        ps.close();
        //construir HISTORIA
        String id ="00000000"+nro;
        int nl=id.length();
        id = id.substring(nl-8);
        return id;
    }    
}


/*CONTENIDO NO EDITADOR 
ESTO ES UNA PRUEBA DEL SERVIDOR 

*/
