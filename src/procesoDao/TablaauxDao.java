package procesoDao;

import ConexionBD.AccesoDB;
import Entidades.TablaauxTo;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import servicios.ICrudService;
import utilitarios.Grupotaux;

public class TablaauxDao implements ICrudService<TablaauxTo> {

    //variables
    Connection cn;
    Statement stm;
    PreparedStatement ps;
    CallableStatement cs;
    ResultSet rs;
    TablaauxTo linea;
    String sql;
    String sp;

    @Override
    public void create(TablaauxTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);  //invoca store procedure, inicia la transaccion
            sql = "insert into tablasaux(grupo,codigo,descripcion) values (?,?,?)";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros       
            ps.setString(1, t.getGrupo());
            ps.setString(2, t.getCodigo());
            ps.setString(3, t.getDescripcion());
            //ejecutar el sp
            ps.executeUpdate();
            ps.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public void update(TablaauxTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false);  //invoca store procedure, inicia la transaccion
            sql = "update tablasaux set descripcion=? where grupo=? and codigo=?";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros
            ps.setString(1, t.getGrupo());
            ps.setString(2, t.getCodigo());
            ps.setString(3, t.getDescripcion());
            //ejecutar el sp
            ps.executeUpdate();
            ps.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public void delete(TablaauxTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sql = "delete from tablasaux where grupo=? and codigo=?";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros           
            ps.setString(1, t.getGrupo());
            ps.setString(1, t.getCodigo());
            //ejecutar el sp
            ps.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public List<TablaauxTo> readAll() throws Exception {
        List<TablaauxTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from tablasaux order by 1,2";
            stm = cn.createStatement();
            rs = stm.executeQuery(sql);
            linea = null;
            while (rs.next()) {
                linea = new TablaauxTo(rs.getString(1), rs.getString(2), rs.getString(3));
                listar.add(linea);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }

    public List<TablaauxTo> readAll(String desc) throws Exception {
        List<TablaauxTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from dbo.tablasaux where grupo=?  order by 3";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, desc);
            rs = ps.executeQuery();
            linea = null;
            while (rs.next()) {
                linea = new TablaauxTo(rs.getString(1), rs.getString(2), rs.getString(3));
                listar.add(linea);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }

    @Override
    public TablaauxTo findForId(Object t) throws Exception {
        return null;
    }

    public TablaauxTo findForId(Object g, Object c) throws Exception {
        linea = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Tablasaux where grupo=? and codigo=?";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, (String) g);
            ps.setString(2, (String) c);
            rs = ps.executeQuery();
            if (rs.next()) {
                linea = new TablaauxTo(rs.getString(1), rs.getString(2), rs.getString(3));
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return linea;
    }

    public String findForName(String descripcion) throws Exception {
        String id = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select codigo from tablasaux  where descripcion='" + descripcion + "'";
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            //cargar los items de rs a la coleccion lista
            if (rs.next()) {
                id = rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return id;
    }

    public String findForId(String x, int grp, int op) throws Exception {
        String dato = null;
        String sqg = null;
        try {
             switch (grp) {
                case Grupotaux.ACO:
                    sqg = " grupo = 'ACO' ";
                    break;
                case Grupotaux.DEP:
                    sqg = " grupo = 'DEP' ";
                    break;
                case Grupotaux.DOC:
                    sqg = " grupo = 'DOC' ";
                    break;
                case Grupotaux.EST:
                    sqg = " grupo = 'EST' ";
                    break;
                case Grupotaux.FUA:
                    sqg = " grupo = 'FUA' ";
                    break;
                case Grupotaux.ING:
                    sqg = " grupo = 'ING' ";
                    break;
                case Grupotaux.PRO:
                    sqg = " grupo = 'PRO' ";
                    break;
                case Grupotaux.SEG:
                    sqg = " grupo = 'SEG' ";
                    break;
                case Grupotaux.SEX:
                    sqg = " grupo = 'SEX' ";
                    break;
                case Grupotaux.TAT:
                    sqg = " grupo = 'TAT' ";
                    break;
            }
            
            switch (op) {
                case 1:
                    sql = "select codigo from tablasaux where "+ sqg+" and descripcion='" + x + "'";
                    break;
                case 2:
                    sql = "select descripcion from tablasaux where "+ sqg+" and codigo='" + x + "'";
                    break;
            }
            //JOptionPane.showMessageDialog(null, "[-ubica->" + sql + "<-]");
         
            cn = AccesoDB.getConnection();
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();
            dato = rs.getString(1);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return dato;
    }

}
