package procesoDao;

import ConexionBD.AccesoDB;
import Entidades.UsuarioTo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import servicios.ICrudService;

public class UsuarioDao implements ICrudService<UsuarioTo> {
    //variables

    Connection cn;
    Statement stm;
    ResultSet rs;
    PreparedStatement ps;
    UsuarioTo usu;
    String sql;

    @Override
    public void create(UsuarioTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sql = "insert into usuarios(usuario,nombre,derechos,acceso1,acceso2,acceso3,acceso4,acceso5,acceso6,acceso7,clave) values(?,?,?,?,?,?,?,?,?,?,?)";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros
            ps.setString(1, t.getUsuario());
            ps.setString(2, t.getNombre());
            ps.setString(3, t.getDerechos());
            ps.setString(4, t.getAcceso1());
            ps.setString(5, t.getAcceso2());
            ps.setString(6, t.getAcceso3());
            ps.setString(7, t.getAcceso4());
            ps.setString(8, t.getAcceso5());
            ps.setString(9, t.getAcceso6());
            ps.setString(10, t.getAcceso7());
            ps.setString(11, t.getClave());
             ps.setString(12, t.getCambia());
            //ejecutar comando
            ps.executeUpdate();
            ps.close();

            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public void update(UsuarioTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            sql = "update usuarios set nombre=?,derechos=?,acceso1=?,acceso2=?,acceso3=?,acceso4=?,acceso5=?,acceso6=?,acceso7=?,clave=?,cambia=? where usuario=?";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros
            ps.setString(1, t.getNombre());
            ps.setString(2, t.getDerechos());
            ps.setString(3, t.getAcceso1());
            ps.setString(4, t.getAcceso2());
            ps.setString(5, t.getAcceso3());
            ps.setString(6, t.getAcceso4());
            ps.setString(7, t.getAcceso5());
            ps.setString(8, t.getAcceso6());
            ps.setString(9, t.getAcceso7());
            ps.setString(10, t.getClave());
            ps.setString(11, t.getCambia());
            ps.setString(12, t.getUsuario());

            //ejecutar comando
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public void delete(UsuarioTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            sql = "delete from usuarios where usuario=?";
            ps = cn.prepareStatement(sql);
            //preparar valores de los parametros           
            ps.setString(1, t.getUsuario());
            //ejecutar comando
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public List<UsuarioTo> readAll() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UsuarioTo findForId(Object t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public UsuarioTo Validar(String usua, String pas) throws Exception {
        usu = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from usuarios where  usuario='" + usua + "' and clave='" + pas + "'";
            //preparar valores de los parametros   
            ps = cn.prepareStatement(sql);

            //ejecutar comando
            rs = ps.executeQuery();
            //cargar los resultados de rs a la coleccion lista
            if (rs.next()) {
                usu = new UsuarioTo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12));
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return usu;
    }
}
