package procesoDao;

import ConexionBD.AccesoDB;
import Entidades.EmergenciaTo;
import Entidades.HistoriaTo;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import servicios.ICrudService;

public class EmergenciaDao implements ICrudService<EmergenciaTo> {
//variables

    Connection cn;
    CallableStatement cs;
    ResultSet rs;
    Statement stm;
    PreparedStatement ps;
    EmergenciaTo eme;
    String sp;
    String sql;

    @Override
    public void create(EmergenciaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call sp_Emergencia_Adicionar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getHistoria());
            cs.setString(2, t.getApelPat());
            cs.setString(3, t.getApelMat());
            cs.setString(4, t.getNombres());
            cs.setString(5, t.getSexo());
            cs.setDate(6, (Date) t.getFecNac());
            cs.setString(7, t.getTpoDoi());
            cs.setString(8, t.getNroDoi());
            cs.setString(9, t.getCodPais());
            cs.setString(10, t.getDireccion());
            cs.setString(11, t.getDistrito());
            cs.setString(12, t.getTelefono());
            cs.setDate(13, (Date) t.getFechaIng());
            cs.setString(14, t.getHoraIng());
            cs.setString(15, t.getCatPac());
            cs.setString(16, t.getNroFua());
            cs.setString(17, t.getGravedad());
            cs.setString(18, t.getPrioridad());
            cs.setString(19, t.getMotIng());
            cs.setString(20, t.getTipAte());
            cs.setString(21, t.getTpoAco());
            cs.setString(22, t.getAcompanante());
            cs.setString(23, t.getAtpoDoi());
            cs.setString(24, t.getAnroDoi());
            cs.setString(25, t.getObservacion());
            cs.setString(26, t.getSecuencia());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public void update(EmergenciaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Emergencia_Actualiza(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getHistoria());
            cs.setString(2, t.getApelPat());
            cs.setString(3, t.getApelMat());
            cs.setString(4, t.getNombres());
            cs.setString(5, t.getSexo());
            cs.setDate(6, (Date) t.getFecNac());
            cs.setString(7, t.getTpoDoi());
            cs.setString(8, t.getNroDoi());
            cs.setString(9, t.getCodPais());
            cs.setString(10, t.getDireccion());
            cs.setString(11, t.getDistrito());
            cs.setString(12, t.getTelefono());
            cs.setDate(13, (Date) t.getFechaIng());
            cs.setString(14, t.getHoraIng());
            cs.setString(15, t.getCatPac());
            cs.setString(16, t.getNroFua());
            cs.setString(17, t.getGravedad());
            cs.setString(18, t.getPrioridad());
            cs.setString(19, t.getMotIng());
            cs.setString(20, t.getTipAte());
            cs.setString(21, t.getTpoAco());
            cs.setString(22, t.getAcompanante());
            cs.setString(23, t.getAtpoDoi());
            cs.setString(24, t.getAnroDoi());
            cs.setString(25, t.getObservacion());
            cs.setString(26, t.getSecuencia());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public void delete(EmergenciaTo t) throws Exception {
        try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Emergencia_Elimina(?,?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setDate(1, (Date) t.getFechaIng());
            cs.setString(2, t.getHistoria());
            cs.setString(3, t.getSecuencia());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public List<EmergenciaTo> readAll() throws Exception {
        List<EmergenciaTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from emergencia order by 13,14";
            stm = cn.createStatement();
            rs = stm.executeQuery(sql);
            eme = null;
            while (rs.next()) {
                eme = new EmergenciaTo(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getDate(6),
                        rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11),
                        rs.getString(12), rs.getDate(13), rs.getString(14), rs.getString(15),
                        rs.getString(16), rs.getString(17), rs.getString(18),
                        rs.getString(19), rs.getString(20), rs.getString(21), rs.getString(22),
                        rs.getString(23), rs.getString(24), rs.getString(25), rs.getString(26));

                listar.add(eme);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;

    }

    @Override
    public EmergenciaTo findForId(Object t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<EmergenciaTo> readAll(String nom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<EmergenciaTo> readAll(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
