package procesoDao;

import ConexionBD.AccesoDB;
import Entidades.PaisTo;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import servicios.ICrudService;

public class PaisDao implements ICrudService<PaisTo> {
    //variables

    Connection cn;
    CallableStatement cs;
    PreparedStatement ps;
    ResultSet rs;
    PaisTo linea;
    String sp;
    String sql;

    @Override
    public void create(PaisTo t) throws Exception {
            try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Paises_Adiciona(?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getCodPais());
            cs.setString(2, t.getNombrePais());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }
    }

    @Override
    public void update(PaisTo t) throws Exception {
       try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Paises_Actualiza(?,?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getCodPais());
            cs.setString(2, t.getNombrePais());
            //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public void delete(PaisTo t) throws Exception {
         try {
            cn = AccesoDB.getConnection();
            cn.setAutoCommit(false); //inicia la transaccion
            sp = "{call dbo.sp_Paises_Elimina(?)}";
            cs = cn.prepareCall(sp);
            //preparar los valores de los parametros del sp
            cs.setString(1, t.getCodPais());
	    //ejecutar el sp
            cs.executeUpdate();
            cs.close();
            cn.commit(); //confirma q la trx se ejecuto ok
        } catch (SQLException e) {
            try {
                cn.rollback(); //deshace la trx
            } catch (SQLException e1) {
            }
            throw e;
        } finally {
            cn.close();
        }

    }

    @Override
    public List<PaisTo> readAll() throws Exception {
        List<PaisTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from paises order by 2";
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            linea = null;
            while (rs.next()) {
                linea = new PaisTo(rs.getString(1), rs.getString(2));
                listar.add(linea);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;

    }

    @Override
    public PaisTo findForId(Object t) throws Exception {
        linea = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from paises where codpais=? ";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, (String) t);
            rs = ps.executeQuery();
            if (rs.next()) {
                linea = new PaisTo(rs.getString(1), rs.getString(2));
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return linea;

    }

    public String findForId(String x, int op) throws Exception {
        String dato = null;
        try {
            switch (op) {
                case 1:
                    sql = "select codpais from paises where nombrepais='" + x + "'";
                    break;
                case 2:
                    sql = "select nombrepais from paises where codpais='" + x + "'";
                    break;
            }
            cn = AccesoDB.getConnection();
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();
            dato = rs.getString(1);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return dato;
    }

//   public PaisTo PaisxId(String id) throws Exception {
//        PaisTo pro = null;
//        try {
//            cn = AccesoDB.getConnection();
//            sql = "select * from paises where codpais=?";
//            ps = cn.prepareStatement(sql);
//            //preparar el valor del parametro
//            ps.setString(1, id);
//            rs = ps.executeQuery();
//            pro = null;
//            if (rs.next()) {
//                pro = new PaisTo(rs.getString(1), rs.getString(2));                
//            }
//            rs.close();
//            ps.close();
//        } catch (SQLException e) {
//            throw e;
//        } finally {
//            cn.close();
//        }
//        return pro;
//    }  
}
