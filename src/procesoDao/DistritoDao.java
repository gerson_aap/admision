package procesoDao;

import ConexionBD.AccesoDB;
import Entidades.DistritoTo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import servicios.ICrudService;

public class DistritoDao implements ICrudService<DistritoTo> {
    //variables

    Connection cn;
    PreparedStatement ps;
    ResultSet rs;
    DistritoTo linea;
    String sql;

    @Override
    public void create(DistritoTo t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(DistritoTo t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(DistritoTo t) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DistritoTo> readAll() throws Exception {
        List<DistritoTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Ubigeo order by 2";
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            linea = null;
            while (rs.next()) {
                linea = new DistritoTo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
                listar.add(linea);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }

    @Override
    public DistritoTo findForId(Object t) throws Exception {
        linea = null;
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Distrito where ubi_geogr=? ";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, (String) t);
            rs = ps.executeQuery();
            if (rs.next()) {
                linea = new DistritoTo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return linea;

    }

    public String findForId(String x, int op) throws Exception {
        String dato = null;
        try {
            switch (op) {
                case 1:
                    sql = "select ubi_geogr from ubigeo where distrito='" + x + "'";
                    break;
                case 2:
                    sql = "select distrito from ubigeo where ubi_geogr='" + x + "'";
                    break;
                case 3:
                    sql = "select Prov from ubigeo where ubi_geogr='" + x + "'";
                    break;
           }
            cn = AccesoDB.getConnection();
            ps = cn.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.next();
            dato = rs.getString(1);
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return dato;
    }

     public List<DistritoTo> readAll(String nombre) throws Exception {
        List<DistritoTo> listar = new ArrayList<>();
        try {
            cn = AccesoDB.getConnection();
            sql = "select * from Ubigeo where distrito like ?";
            ps = cn.prepareStatement(sql);
            //preparar el valor del parametro
            ps.setString(1, nombre + "%");
            rs = ps.executeQuery();
            linea = null;
            while (rs.next()) {
                linea = new DistritoTo(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4));
                listar.add(linea);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            throw e;
        } finally {
            cn.close();
        }
        return listar;
    }
    
}
