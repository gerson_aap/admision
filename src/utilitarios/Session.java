
package utilitarios;

import java.util.HashMap;
import java.util.Map;


public class Session {
     //atributos
  private static Map<String, Object> datos;
 
  //inicializados static
  static {
    datos = new HashMap<>();
  }
//metodos
  public static void put(String key, Object value) {
    datos.put(key, value);
  }

  public static Object get(String key) {
    return datos.get(key);
  }

}
