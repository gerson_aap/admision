package utilitarios;

public class Grupotaux {
    //definir constante

    public static final int ACO = 1;
    public static final int APG = 2;
    public static final int DEP = 3;
    public static final int DOC = 4;
    public static final int EST = 5;
    public static final int FUA = 6;
    public static final int ING = 7;
    public static final int LAT = 8;
    public static final int NIV = 9;
    public static final int PER = 10;
    public static final int PRO = 11;
    public static final int SAL = 12;
    public static final int SCT = 13;
    public static final int SEG = 14;
    public static final int SEX = 15;
    public static final int TAT = 16;
    public static final int TOP = 17;

}
